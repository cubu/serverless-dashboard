FROM mhart/alpine-node:12.16.3 AS base
ARG NODE_ENV
ENV NODE_ENV ${NODE_ENV}
ENV HOME /app
WORKDIR ${HOME}
COPY package.json ${HOME}/

FROM base AS dependencies
COPY package-lock.json .babelrc ${HOME}/
RUN apk add g++ make python
RUN npm set progress=false && npm config set depth 0
RUN npm install
COPY . ${HOME}/

FROM dependencies AS development
RUN apk --no-cache add openjdk11 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
RUN npm install -g serverless
ENTRYPOINT [ "./entrypoint.sh" ]
