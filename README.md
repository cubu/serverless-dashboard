# Dashboard Serverless API

API powered by `Serverless Framework` that allows user to sign up and authenticate to be able to create solutions, screens and widgets.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Running the stack

#### Prerequisites

Before running it you should prepare the environment variables.

Create a copy of services.envar.dist named services.envar and set the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` to valid values.

To get a redis server, a dynamodb and the api running you should run at the project root folder:

```
sudo bash run-docker.sh
```

This will set 3 docker containers:
 - Redis container at `localhost:6379`
 - DynamoDB at `localhost:8000`and the shell at `localhost:8000/shell`
 - Dashboard API at `localhost:3000/dev`
 
After this you will be set up to develop or test the app.

The Dashboard container will start the serverless command which will initialize the tables at the dynamoDB container, and will listen to the incoming requests at `localhost:3000/dev`. 

You can visit `localhost:3000/dev/api-docs/api-docs` in order to see a SwaggerUI that will let you try the endpoints.


#### Running without docker
Before running it you should change REDIS_HOST and DYNAMODB_ENDPOINT environment variables to localhost instead of the name of the docker containers.

If for some reason you want to ignore docker you could always run:

```
npm install -g serverless
npm install
sls offline start --skipCacheInvalidation
```

Serverless DynamoDB Local plugin might need a Java Runtime Engine (JRE) installed in your machine.

### Endpoints

:unlock:`POST: localhost:3000/dev/user/`:

This endpoint will create a user in the usersTable at the DynamoDB docker container instance saving its email, name, hashed password and the salt used. It will also add a key value pair (key: token, value: email) to Redis that will be used to authenticate the user in further endpoints.

:unlock:`POST: localhost:3000/dev/user/authorize`:

This endpoint expects an email and a password, finds the user in the database and if the password matches it adds the key value pair to Redis.

:unlock:`DELETE: localhost:3000/dev/user/`:

This endpoint expects an email and a password, tries to find the user and if the password matches tries to delete the user.

:lock:`POST: localhost:3000/dev/solution`:

This endpoint expects a body with the data and a token that will be searched in Redis to authenticate the user, if the token matches it will add the user to the request then it will create a solution from the body received.

:lock:`DELETE: localhost:3000/dev/solution`:

This endpoint expects the token as before, and an id that will be used to try to delete the solution in the DynamoDB container.

:lock:`POST: localhost:3000/dev/screen`:

This endpoint expects a body with the data and a token that will be searched in Redis to authenticate the user, if the token matches it will add the user to the request then it will create a screen from the body received.

:lock:`DELETE: localhost:3000/dev/screen`:

This endpoint expects the token as before, and an id that will be used to try to delete the screen in the DynamoDB container.

:lock:`POST: localhost:3000/dev/widget`:

This endpoint expects a body with the data and a token that will be searched in Redis to authenticate the user, if the token matches it will add the user to the request then it will create a widget from the body received.

:lock:`DELETE: localhost:3000/dev/widget`:

This endpoint expects the token as before, and an id that will be used to try to delete the widget in the DynamoDB container.


#### Notes: 

All the endpoints have integrated `@hapi/joi` validation implicitly that will verify that all the current api calls have the required parameters and that they fit the requirements.

All the authorized endpoints will try to validate the user through searching the token received in Redis. The token should be sent in an `Authorization` Header with a valid value, as can we tried in the SwaggerUI.

The schemas for DynamoDB maybe aren't optimized since being the first time that I had to define tables, I went straight forward defining them.

## Running the tests

To run the test suite run at the project root folder:

```
npm install
npm run test
```

### To do

- The tests and coverage of the api could certainly be improved by adding more meaningful tests.

- Integration test, serverless provides the 'serverless.test.yml` to define a battery of tests, but the service must be deployed in AWS to run.

- Permissions for the users, right now every user can do every action. But since this is only an excuse to try serverless framework it's ok.

- Wanted to add dependency injection with `typedi` but my time went to fiddle with Serverless.

- Add some endpoints to get the data from DynamoDB.

- Modify server.js to be able to deploy in production as a dockerized container, instead of using sls deploy.

### Notes

I wasn't sure about creating three tables one for Solution, Screen and Widgets. Ultimately I decided to separate them to have more than one endpoint that is able to update every part of the solution. 

Although I think it could be done, much like Grafana does with his dashboards which are a unique JSON file which simple defines the widgets and which prometheus metrics are related.

## Deployment

Thanks to `Serverless Framework` just by adding the right configuration to `serverless.yml` we could deploy this to AWS with one command:

`sls deploy`

The current configuration will map all the Dashboard API to a single AWS Lambda function named app.

:exclamation:Disclaimer: I haven't tried it, I should read a little bit more of the serverless documentation before moving forward.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
