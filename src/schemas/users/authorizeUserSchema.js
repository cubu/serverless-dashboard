import Joi from '@hapi/joi';

const authorizeUserSchema = Joi.object({
  email: Joi.string().required(),
  password: Joi.string().required(),
});

export default authorizeUserSchema;
