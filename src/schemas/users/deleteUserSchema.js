import Joi from '@hapi/joi';

const deleteUserSchema = Joi.object({
  email: Joi.string().required(),
  password: Joi.string().required(),
});

export default deleteUserSchema;
