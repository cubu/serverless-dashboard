import Joi from '@hapi/joi';

const createOrUpdateScreenSchema = Joi.object({
  id: Joi.string(),
  name: Joi.string().required(),
  widgetsAvailable: Joi.array().items(Joi.string().uuid()).required(),
});

export default createOrUpdateScreenSchema;
