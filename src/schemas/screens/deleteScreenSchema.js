import Joi from '@hapi/joi';

const deleteScreenSchema = Joi.object({
  id: Joi.string().uuid().required(),
});

export default deleteScreenSchema;
