import createUserSchema from './users/createUserSchema';
import authorizeUserSchema from './users/authorizeUserSchema';
import deleteUserSchema from './users/deleteUserSchema';
import createOrUpdateSolutionSchema from './solutions/createOrUpdateSolutionSchema';
import createOrUpdateWidgetSchema from './widgets/createOrUpdateWidgetSchema';
import createOrUpdateScreenSchema from './screens/createOrUpdateScreenSchema';
import deleteSolutionSchema from './solutions/deleteSolutionSchema';
import deleteScreenSchema from './screens/deleteScreenSchema';
import deleteWidgetSchema from './widgets/deleteWidgetSchema';

export {
  createUserSchema,
  authorizeUserSchema,
  deleteUserSchema,
  createOrUpdateSolutionSchema,
  createOrUpdateWidgetSchema,
  createOrUpdateScreenSchema,
  deleteSolutionSchema,
  deleteScreenSchema,
  deleteWidgetSchema,
};