import Joi from '@hapi/joi';

const deleteWidgetSchema = Joi.object({
  id: Joi.string().uuid().required(),
});

export default deleteWidgetSchema;
