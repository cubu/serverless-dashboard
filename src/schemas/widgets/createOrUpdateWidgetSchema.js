import Joi from '@hapi/joi';

const createOrUpdateWidgetSchema = Joi.object({
  id: Joi.string(),
  name: Joi.string().required(),
  metric: Joi.string().required(),
  kind: Joi.string().valid('Pie', 'Bar', 'Chart'),
});

export default createOrUpdateWidgetSchema;
