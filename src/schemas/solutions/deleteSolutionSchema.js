import Joi from '@hapi/joi';

const deleteSolutionSchema = Joi.object({
  id: Joi.string().uuid().required(),
});

export default deleteSolutionSchema;
