import Joi from '@hapi/joi';

const createOrUpdateSolutionSchema = Joi.object({
  id: Joi.string(),
  name: Joi.string().required(),
  screensAvailable: Joi.array().items(Joi.string()).required(),
});

export default createOrUpdateSolutionSchema;
