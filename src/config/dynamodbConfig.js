const { DYNAMODB_ENDPOINT, AWS_REGION } = process.env;

const dynamodbConfig = {
  endpoint: DYNAMODB_ENDPOINT,
  region: AWS_REGION,
};

export default dynamodbConfig;