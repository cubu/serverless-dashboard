import serverConfig from './serverConfig';
import dynamodbConfig from './dynamodbConfig';
import redisConfig from './redisConfig';

export { serverConfig, dynamodbConfig, redisConfig };