/**
 * @swagger
 * definitions:
 *   Solution:
 *     type: object
 *     properties:
 *       name:
 *         type: string
 *       screensAvailable:
 *         type: array
 *         items:
 *           type: string
 *   Screen:
 *     type: object
 *     properties:
 *       name:
 *         type: string
 *       widgetsAvailable:
 *         type: array
 *         items:
 *           type: string
 *   Widget:
 *     type: object
 *     properties:
 *       name:
 *         type: string
 *       metric:
 *         type: string
 *       kind:
 *         type: string
 *   CreateUser:
 *     type: object
 *     properties:
 *       name:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *   User:
 *     type: object
 *     properties:
 *       email:
 *         type: string
 *       password:
 *         type: string
 */

/**
 * @swagger
 * /user/:
 *   post:
 *     tags:
 *     - "user"
 *     summary: "Create User"
 *     produces:
 *      - application/json
 *     parameters:
 *     - in: "body"
 *       name: "body"
 *       required: true
 *       schema:
 *         $ref: "#/definitions/CreateUser"
 *     responses:
 *       200:
 *         description: Response ok.
 *       5XX:
 *         description: Unexpected internal error.
 *   delete:
 *     tags:
 *     - "user"
 *     summary: "Delete User"
 *     produces:
 *      - application/json
 *     parameters:
 *     - in: "body"
 *       name: "body"
 *       required: true
 *       schema:
 *         $ref: "#/definitions/User"
 *     responses:
 *       200:
 *         description: Response ok.
 *       5XX:
 *         description: Unexpected internal error.
 * /user/authorize:
 *   post:
 *     tags:
 *     - "user"
 *     summary: "Authorize User"
 *     produces:
 *      - application/json
 *     parameters:
 *     - in: "body"
 *       name: "body"
 *       required: true
 *       schema:
 *         $ref: "#/definitions/User"
 *     responses:
 *       200:
 *         description: Response ok.
 *       5XX:
 *         description: Unexpected internal error.
 * /solution/{id}:
 *   post:
 *     tags:
 *     - "solution"
 *     summary: "Create or update solution"
 *     security:
 *      - Bearer: []
 *      - Token: []
 *     produces:
 *      - application/json
 *     parameters:
 *     - name: id
 *       in: "path"
 *       description: Id of solution
 *       type: string
 *     - in: "body"
 *       name: "body"
 *       required: true
 *       schema:
 *         $ref: "#/definitions/Solution"
 *     responses:
 *       200:
 *         description: Response ok.
 *       401:
 *         description: Unauthorized
 *       5XX:
 *         description: Unexpected internal error.
 *   delete:
 *     tags:
 *     - "solution"
 *     summary: "Delete solution"
 *     security:
 *      - Bearer: []
 *      - Token: []
 *     produces:
 *      - application/json
 *     parameters:
 *     - name: id
 *       in: "path"
 *       description: Id of solution
 *       required: true
 *       type: string
 *     responses:
 *       200:
 *         description: Response ok.
 *       401:
 *         description: Unauthorized
 *       5XX:
 *         description: Unexpected internal error.
 * /screen/{id}:
 *   post:
 *     tags:
 *     - "screen"
 *     summary: "Create or update screen"
 *     security:
 *      - Bearer: []
 *      - Token: []
 *     produces:
 *      - application/json
 *     parameters:
 *     - name: id
 *       in: "path"
 *       description: Id of screen
 *       type: string
 *     - in: "body"
 *       name: "body"
 *       required: true
 *       schema:
 *         $ref: "#/definitions/Screen"
 *     responses:
 *       200:
 *         description: Response ok.
 *       401:
 *         description: Unauthorized
 *       5XX:
 *         description: Unexpected internal error.
 *   delete:
 *     tags:
 *     - "screen"
 *     summary: "Delete screen"
 *     security:
 *      - Bearer: []
 *      - Token: []
 *     produces:
 *      - application/json
 *     parameters:
 *     - name: id
 *       in: "path"
 *       description: Id of screen
 *       required: true
 *       type: string
 *     responses:
 *       200:
 *         description: Response ok.
 *       401:
 *         description: Unauthorized
 *       5XX:
 *         description: Unexpected internal error.
 * /widget/{id}:
 *   post:
 *     tags:
 *     - "widget"
 *     summary: "Create or update widget"
 *     security:
 *      - Bearer: []
 *      - Token: []
 *     produces:
 *      - application/json
 *     parameters:
 *     - name: id
 *       in: "path"
 *       description: Id of widget
 *       type: string
 *     - in: "body"
 *       name: "body"
 *       required: true
 *       schema:
 *         $ref: "#/definitions/Widget"
 *     responses:
 *       200:
 *         description: Response ok.
 *       401:
 *         description: Unauthorized
 *       5XX:
 *         description: Unexpected internal error.
 *   delete:
 *     tags:
 *     - "widget"
 *     summary: "Delete widget"
 *     security:
 *      - Bearer: []
 *      - Token: []
 *     produces:
 *      - application/json
 *     parameters:
 *     - name: id
 *       in: "path"
 *       description: Id of widget
 *       required: true
 *       type: string
 *     responses:
 *       200:
 *         description: Response ok.
 *       401:
 *         description: Unauthorized
 *       5XX:
 *         description: Unexpected internal error.
 */
