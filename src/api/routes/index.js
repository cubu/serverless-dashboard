import express from 'express';
import { createUserHandler, authorizeUserHandler, deleteUserHandler, createOrUpdateSolutionHandler, createOrUpdateScreenHandler, createOrUpdateWidgetHandler, deleteSolutionHandler, deleteScreenHandler, deleteWidgetHandler } from 'api/handlers';
import { authorization } from 'api/middlewares';

const router = new express.Router();

router
  .route('/user')
  .post(createUserHandler)
  .delete(deleteUserHandler);

router
  .route('/user/authorize')
  .post(authorizeUserHandler);

router
  .route('/solution/:id')
  .all(authorization)
  .post(createOrUpdateSolutionHandler)
  .delete(deleteSolutionHandler);

router
  .route('/screen/:id')
  .all(authorization)
  .post(createOrUpdateScreenHandler)
  .delete(deleteScreenHandler);

router
  .route('/widget/:id')
  .all(authorization)
  .post(createOrUpdateWidgetHandler)
  .delete(deleteWidgetHandler);

export default router;
