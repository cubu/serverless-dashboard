import RedisService from 'services/RedisService';

const authorization = async (req, res, next) => {
  const authHeader = req.header('Authorization');

  if (authHeader) {
    const userEmail = JSON.parse(await RedisService.getElement({ key: authHeader }));
    
    if(userEmail) {
      req.currentUser = userEmail;
      next();
    } else {
      res.sendStatus(401);
    }
  } else {
    res.sendStatus(401);
  }
};

export default authorization;