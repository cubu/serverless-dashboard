import genericHandler from '../genericHandler';
import { blCreateOrUpdateScreen } from 'services';
import { createOrUpdateScreenSchema } from 'schemas';

const createOrUpdateScreenHandler = async (req, res, next) => {
  const { body: { name, widgetsAvailable }, params: { id }, currentUser } = req;

  genericHandler({
    blFunction: blCreateOrUpdateScreen,
    res,
    req,
    next,
    schema: createOrUpdateScreenSchema,
  })({ id, name, widgetsAvailable, currentUser });
};

export default createOrUpdateScreenHandler;
