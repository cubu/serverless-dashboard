import genericHandler from '../genericHandler';
import { blDeleteScreen } from 'services';
import { deleteScreenSchema } from 'schemas';

const deleteScreenHandler = async (req, res, next) => {
  const { params: { id } } = req;

  genericHandler({
    blFunction: blDeleteScreen,
    res,
    req,
    next,
    schema: deleteScreenSchema,
  })({ id });
};

export default deleteScreenHandler;
