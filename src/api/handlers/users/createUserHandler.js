import genericHandler from '../genericHandler';
import { blCreateUser } from 'services';
import { createUserSchema } from 'schemas';

const createUserHandler = async (req, res, next) => {
  const { body: { name, email, password } } = req;

  genericHandler({
    blFunction: blCreateUser,
    res,
    req,
    next,
    schema: createUserSchema,
  })({ name, email, password });
};

export default createUserHandler;
