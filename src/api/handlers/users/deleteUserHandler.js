import genericHandler from '../genericHandler';
import { blDeleteUser } from 'services';
import { deleteUserSchema } from 'schemas';

const deleteUserHandler = async (req, res, next) => {
  const { body: { email, password } } = req;

  genericHandler({
    blFunction: blDeleteUser,
    res,
    req,
    next,
    schema: deleteUserSchema,
  })({ email, password });
};

export default deleteUserHandler;
