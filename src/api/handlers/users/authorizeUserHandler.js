import genericHandler from '../genericHandler';
import { blAuthorizeUser } from 'services';
import { authorizeUserSchema } from 'schemas';

const authorizeUserHandlers = async (req, res, next) => {
  const { body: { email, password } } = req;

  genericHandler({
    blFunction: blAuthorizeUser,
    res,
    req,
    next,
    schema: authorizeUserSchema,
  })({ email, password });
};

export default authorizeUserHandlers;
