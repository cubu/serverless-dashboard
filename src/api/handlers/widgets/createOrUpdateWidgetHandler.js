import genericHandler from '../genericHandler';
import { blCreateOrUpdateWidget } from 'services';
import { createOrUpdateWidgetSchema } from 'schemas';

const createOrUpdateWidgetHandler = async (req, res, next) => {
  const { body: { name, metric, kind }, params: { id }, currentUser } = req;

  genericHandler({
    blFunction: blCreateOrUpdateWidget,
    res,
    req,
    next,
    schema: createOrUpdateWidgetSchema,
  })({ id, name, metric, kind, currentUser });
};

export default createOrUpdateWidgetHandler;
