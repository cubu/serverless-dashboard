import genericHandler from '../genericHandler';
import { blDeleteWidget } from 'services';
import { deleteWidgetSchema } from 'schemas';

const deleteWidgetHandler = async (req, res, next) => {
  const { params: { id } } = req;

  genericHandler({
    blFunction: blDeleteWidget,
    res,
    req,
    next,
    schema: deleteWidgetSchema,
  })({ id });
};

export default deleteWidgetHandler;
