import genericHandler from '../genericHandler';
import { blDeleteSolution } from 'services';
import { deleteSolutionSchema } from 'schemas';

const deleteSolutionHandler = async (req, res, next) => {
  const { params: { id } } = req;

  genericHandler({
    blFunction: blDeleteSolution,
    res,
    req,
    next,
    schema: deleteSolutionSchema,
  })({ id });
};

export default deleteSolutionHandler;
