import genericHandler from '../genericHandler';
import { blCreateOrUpdateSolution } from 'services';
import { createOrUpdateSolutionSchema } from 'schemas';

const createOrUpdateSolutionHandler = async (req, res, next) => {
  const { body: { name, screensAvailable }, params: { id }, currentUser } = req;

  genericHandler({
    blFunction: blCreateOrUpdateSolution,
    res,
    req,
    next,
    schema: createOrUpdateSolutionSchema,
  })({ id, name, screensAvailable, currentUser });
};

export default createOrUpdateSolutionHandler;
