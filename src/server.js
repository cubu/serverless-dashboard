import 'core-js/stable';
import 'regenerator-runtime/runtime';
import express from 'express';
import bodyParser from 'body-parser';
import { apiDocs } from 'loaders';
import routes from 'api/routes';
import serverless from 'serverless-http';

const app = express();

app.use(bodyParser.json());

app.get('/', (req, res, next) => {
  res.send('API is running...');
  next();
});
 
app.get('/ping', (req, res, next) => {
  res.send('ponng');

  next();
});

apiDocs(app);

app.use('/', routes);

app.use((err, req, res, next) => {
  const { statusCode = 500, message = '' } = err;

  res.status(statusCode).json({ error: message });
  next();
});

const handler = serverless(app);

export { handler };