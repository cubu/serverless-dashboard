import argon2 from 'argon2';
import DynamoDbService from 'services/DynamoDbService';

const deleteUser = async ({ email, password }) => {
  const key = { 'email': { S: email } };
  const tableName = 'usersTable';

  const userRecord = await DynamoDbService.getItem({ key, tableName });

  if (!userRecord?.Item?.email) {
    throw new Error('User not registered');
  }
  const validPassword = await argon2.verify(userRecord.Item.password.S, password);

  if(validPassword) {
    DynamoDbService.deleteItem({ key, tableName });
    
    return 'User deleted';
  } else {
    throw new Error('Invalid Password');
  }
};


export default deleteUser;