import { randomBytes } from 'crypto';
import argon2 from 'argon2';
import DynamoDbService from 'services/DynamoDbService';

const createUser = async ({ name, email, password }) => {
  const salt = randomBytes(32);
  const hashedPassword = await argon2.hash(password, { salt });
  const userItem = {
    'email': { S: email },
    'password': { S: hashedPassword },
    'salt': { S: salt.toString() },
    'name': { S: name },

  };
  const tableName = 'usersTable';

  await DynamoDbService.putItem({ item: userItem, tableName });


  const key = { 'email': { S: email } };

  const userRecord = await DynamoDbService.getItem({ key, tableName });
  
  if (!userRecord.Item.email) {
    throw new Error('User cannot be created');
  }

  const token = randomBytes(32).toString('hex');
  
  return { name, email, token };
};


export default createUser;