import deleteUser from '../deleteUser';
import DynamoDbServiceMock from '../../../services/DynamoDbService';

jest.mock('../../../services/DynamoDbService');

const params = {
  email: 'pepe@pepe.com',
  password: 'pepePassword',
};
const hashedPassword = '$argon2i$v=19$m=4096,t=3,p=1$RTRTER7iH6iKq9cNK1nfdrgR9ApJbCdDH7EiCzRbpT0$azE/pdSQifUs4DLsw0rcVGHG8lmdR/+D7snZW/Ek1bc';

describe('deleteUser', () => {
  it('Should call dynamo db to get user and delete user', async () => {
    const itemParams = { key: { email: { S: params.email }}, tableName: 'usersTable'};
    
    DynamoDbServiceMock.getItem.mockReturnValue({ Item: { email: { S: params.email }, password: { S: hashedPassword } } });
    await deleteUser(params);

    expect(DynamoDbServiceMock.getItem).toBeCalledWith(expect.objectContaining(itemParams));
    expect(DynamoDbServiceMock.deleteItem).toBeCalledWith(expect.objectContaining(itemParams));
  });

  
});
