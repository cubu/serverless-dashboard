import createUser from '../createUser';
import DynamoDbServiceMock from '../../../services/DynamoDbService';

jest.mock('../../../services/DynamoDbService');

const params = {
  name: 'pepe',
  email: 'pepe@pepe.com',
  password: 'pepePassword',
};

describe('createUser', () => {
  it('Should call dynamo db to create user', async () => {
    const getItemParams = { key: { email: { S: params.email }}, tableName: 'usersTable'};


    DynamoDbServiceMock.getItem.mockReturnValue({ Item: { email: { S: params.email } } });
    await createUser(params);

    expect(DynamoDbServiceMock.putItem).toBeCalledWith(expect.objectContaining({ tableName: 'usersTable'}));
    expect(DynamoDbServiceMock.getItem).toBeCalledWith(expect.objectContaining(getItemParams));
  });

  
});
