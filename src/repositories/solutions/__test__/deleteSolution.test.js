import deleteSolution from '../deleteSolution';
import DynamoDbServiceMock from '../../../services/DynamoDbService';

jest.mock('../../../services/DynamoDbService');

const params = {
  id: 'uuid',
};

describe('deleteSolution', () => {
  it('Should call dynamo db to delete solution', async () => {

    await deleteSolution(params);

    expect(DynamoDbServiceMock.deleteItem).toBeCalled();
  });

  
});
