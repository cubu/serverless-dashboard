import createOrUpdateSolution from '../createOrUpdateSolution';
import DynamoDbServiceMock from '../../../services/DynamoDbService';

jest.mock('../../../services/DynamoDbService');

const params = {
  id: 'uuid',
  name: 'pepe@pepe.com',
  screensAvailable: ['string'],
  currentUser: 'pepe@mail.com',
};

describe('createOrUpdateSolution', () => {
  it('Should call dynamo db to create solution', async () => {

    await createOrUpdateSolution(params);

    expect(DynamoDbServiceMock.updateItem).toBeCalled();
  });

  
});
