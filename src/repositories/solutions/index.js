import createOrUpdateSolution from './createOrUpdateSolution';
import deleteSolution from './deleteSolution';

export { createOrUpdateSolution, deleteSolution };