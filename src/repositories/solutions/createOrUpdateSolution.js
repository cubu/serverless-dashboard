import DynamoDbService from 'services/DynamoDbService';
import { uuid, isUuid } from 'uuidv4';

const createOrUpdateSolution = async ({ id, name, screensAvailable, currentUser }) => {
  const solutionId = isUuid(id) ? id : uuid();
  const tableName = 'solutionsTable';
  const key = { 'id': { S: solutionId } };
  const updateExpression = 'set solution_name=:n, screens_available=:sa, modified_by=:mb';
  const expressionAttributeValues = {
    ':n': { S: name },
    ':sa': { SS: screensAvailable },
    ':mb': { S: currentUser },
  };

  await DynamoDbService.updateItem({ key, updateExpression, expressionAttributeValues, tableName });

  return solutionId;
};


export default createOrUpdateSolution;