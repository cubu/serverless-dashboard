import DynamoDbService from 'services/DynamoDbService';

const deleteSolution = async ({ id }) => {
  const key = { 'id': { S: id } };
  const tableName = 'solutionsTable';

  DynamoDbService.deleteItem({ key, tableName });
};


export default deleteSolution;