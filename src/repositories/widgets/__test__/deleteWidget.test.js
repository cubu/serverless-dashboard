import deleteWidget from '../deleteWidget';
import DynamoDbServiceMock from '../../../services/DynamoDbService';

jest.mock('../../../services/DynamoDbService');

const params = {
  id: 'uuid',
};

describe('deleteWidget', () => {
  it('Should call dynamo db to delete widget', async () => {

    await deleteWidget(params);

    expect(DynamoDbServiceMock.deleteItem).toBeCalled();
  });

  
});
