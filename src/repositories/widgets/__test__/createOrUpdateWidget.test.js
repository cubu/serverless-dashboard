import createOrUpdateWidget from '../createOrUpdateWidget';
import DynamoDbServiceMock from '../../../services/DynamoDbService';

jest.mock('../../../services/DynamoDbService');

const params = {
  id: 'uuid',
  name: 'pepe@pepe.com',
  metric: 'pepemetric',
  kind: 'Bar',
  currentUser: 'pepe@mail.com',
};

describe('createOrUpdateWidget', () => {
  it('Should call dynamo db to create widget', async () => {

    await createOrUpdateWidget(params);

    expect(DynamoDbServiceMock.updateItem).toBeCalled();
  });

  
});
