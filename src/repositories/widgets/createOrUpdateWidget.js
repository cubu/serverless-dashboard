import DynamoDbService from 'services/DynamoDbService';
import { uuid, isUuid } from 'uuidv4';

const createOrUpdateWidget = async ({ id, name, metric, kind, currentUser }) => {
  const widgetId = isUuid(id) ? id : uuid();
  const tableName = 'widgetsTable';
  const key = { 'id': { S: widgetId } };
  const updateExpression = 'set widget_name=:n, metric=:m, kind=:k, modified_by=:mb';
  const expressionAttributeValues = {
    ':n': { S: name },
    ':m': { S: metric },
    ':k': { S: kind },
    ':mb': { S: currentUser },
  };

  await DynamoDbService.updateItem({ key, updateExpression, expressionAttributeValues, tableName });

  return widgetId;
};


export default createOrUpdateWidget;