import createOrUpdateWidget from './createOrUpdateWidget';
import deleteWidget from './deleteWidget';

export { createOrUpdateWidget, deleteWidget };