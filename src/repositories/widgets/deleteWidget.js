import DynamoDbService from 'services/DynamoDbService';

const deleteWidget = async ({ id }) => {
  const key = { 'id': { S: id } };
  const tableName = 'widgetsTable';

  DynamoDbService.deleteItem({ key, tableName });
};


export default deleteWidget;