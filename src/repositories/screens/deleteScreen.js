import DynamoDbService from 'services/DynamoDbService';

const deleteScreen = async ({ id }) => {
  const key = { 'id': { S: id } };
  const tableName = 'screensTable';

  DynamoDbService.deleteItem({ key, tableName });
};


export default deleteScreen;