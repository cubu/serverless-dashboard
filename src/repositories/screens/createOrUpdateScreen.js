import DynamoDbService from 'services/DynamoDbService';
import { uuid, isUuid } from 'uuidv4';

const createOrUpdateScreen = async ({ id, name, widgetsAvailable, currentUser }) => {
  const screenId = isUuid(id) ? id : uuid();
  const tableName = 'screensTable';
  const key = { 'id': { S: screenId } };
  const updateExpression = 'set screen_name=:n, widgets_available=:wa, modified_by=:mb';
  const expressionAttributeValues = {
    ':n': { S: name },
    ':wa': { SS: widgetsAvailable },
    ':mb': { S: currentUser },
  };

  await DynamoDbService.updateItem({ key, updateExpression, expressionAttributeValues, tableName });

  return screenId;
};


export default createOrUpdateScreen;