import deleteScreen from '../deleteScreen';
import DynamoDbServiceMock from '../../../services/DynamoDbService';

jest.mock('../../../services/DynamoDbService');

const params = {
  id: 'uuid',
};

describe('deleteScreen', () => {
  it('Should call dynamo db to delete screen', async () => {

    await deleteScreen(params);

    expect(DynamoDbServiceMock.deleteItem).toBeCalled();
  });
  
});
