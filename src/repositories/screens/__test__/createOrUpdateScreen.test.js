import createOrUpdateScreen from '../createOrUpdateScreen';
import DynamoDbServiceMock from '../../../services/DynamoDbService';

jest.mock('../../../services/DynamoDbService');

const params = {
  id: 'uuid',
  name: 'pepe@pepe.com',
  widgetsAvailable: ['string'],
  currentUser: 'pepe@mail.com',
};

describe('createOrUpdateScreen', () => {
  it('Should call dynamo db to create screen', async () => {

    await createOrUpdateScreen(params);

    expect(DynamoDbServiceMock.updateItem).toBeCalled();
  });

  
});
