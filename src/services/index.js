import blCreateUser from './user/blCreateUser.js';
import blDeleteUser from './user/blDeleteUser.js';
import blAuthorizeUser from './user/blAuthorizeUser.js';
import blCreateOrUpdateSolution from './solution/blCreateOrUpdateSolution';
import blCreateOrUpdateScreen from './screen/blCreateOrUpdateScreen';
import blCreateOrUpdateWidget from './widget/blCreateOrUpdateWidget';
import blDeleteSolution from './solution/blDeleteSolution';
import blDeleteScreen from './screen/blDeleteScreen';
import blDeleteWidget from './widget/blDeleteWidget';

export {
  blCreateUser,
  blAuthorizeUser,
  blDeleteUser,
  blCreateOrUpdateSolution,
  blCreateOrUpdateWidget,
  blCreateOrUpdateScreen,
  blDeleteSolution,
  blDeleteScreen,
  blDeleteWidget,
};