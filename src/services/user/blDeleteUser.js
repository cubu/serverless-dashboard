import { deleteUser } from 'repositories/users';

const blDeleteUser = async ({ email, password }) => {
  await deleteUser({ email, password });
};


export default blDeleteUser;