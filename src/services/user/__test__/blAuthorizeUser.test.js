import blAuthorizeUser from '../blAuthorizeUser';
import DynamoDbServiceMock from '../../DynamoDbService';
import RedisServiceMock from '../../RedisService';

jest.mock('../../RedisService');
jest.mock('../../DynamoDbService');

const params = {
  email: 'pepe@pepe.com',
  password: 'pepePassword',
};

describe('blAuthorizeUser', () => {
  it('Should authorize user', async () => {
    const setElementParams = { value: params.email, ttl: 60 * 60 * 10 };
    const getItemParams = { key: { email: { S: params.email }}, tableName: 'usersTable'};

    DynamoDbServiceMock.getItem.mockReturnValue({ Item: { email: { S: params.email }, password: { S: '$argon2i$v=19$m=4096,t=3,p=1$RTRTER7iH6iKq9cNK1nfdrgR9ApJbCdDH7EiCzRbpT0$azE/pdSQifUs4DLsw0rcVGHG8lmdR/+D7snZW/Ek1bc'} } });
    await blAuthorizeUser(params);

    expect(DynamoDbServiceMock.getItem).toBeCalledWith(expect.objectContaining(getItemParams));
    expect(RedisServiceMock.setElement).toBeCalledWith(expect.objectContaining(setElementParams));
  });

  
});
