import blDeleteUser from '../blDeleteUser';
import userRepositoriesMock from '../../../repositories/users';

const params = {
  email: 'pepe@pepe.com',
  password: 'pepe',
};

jest.mock('../../../repositories/users', () => ({
  deleteUser: jest.fn().mockResolvedValue({}),
}));


describe('blDeleteUser', () => {
  it('Should Delete user', async () => {
    await blDeleteUser(params);

    expect(userRepositoriesMock.deleteUser).toBeCalledWith(expect.objectContaining({ password: params.password, email: params.email }));
  });

  
});
