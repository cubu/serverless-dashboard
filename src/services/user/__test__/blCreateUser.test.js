import blCreateUser from '../blCreateUser';
import RedisServiceMock from '../../RedisService';
import userRepositoriesMock from '../../../repositories/users';

jest.mock('../../RedisService');
jest.mock('../../DynamoDbService');

const params = {
  name: 'Pepe',
  email: 'pepe@pepe.com',
  password: 'pepepassword',
  token: '3206e27cef7a206c8126421fd80c70a4ab176fd2c61c17092fcf53bf58735321',
};

jest.mock('../../../repositories/users', () => ({
  createUser: jest.fn().mockResolvedValue({ name: 'pepe@pepe.com', email: 'pepe@pepe.com', token: '3206e27cef7a206c8126421fd80c70a4ab176fd2c61c17092fcf53bf58735321' }),
}));


describe('blCreateUser', () => {
  it('Should create user', async () => {
    const setElementParams = { value: params.email, ttl: 60 * 60 * 10 };

    await blCreateUser(params);

    expect(userRepositoriesMock.createUser).toBeCalledWith(expect.objectContaining({ name: params.name, email: params.email, password: params.password }));
    expect(RedisServiceMock.setElement).toBeCalledWith(expect.objectContaining(setElementParams));
  });

  
});
