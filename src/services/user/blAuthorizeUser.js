import argon2 from 'argon2';
import { randomBytes } from 'crypto';
import RedisService from '../RedisService';
import DynamoDbService from '../DynamoDbService';

const blAuthorizeUser = async ({ email, password }) => {
  const key = { 'email': { S: email } };
  const tableName = 'usersTable';

  const userRecord = await DynamoDbService.getItem({ key, tableName });

  if (!userRecord?.Item?.email) {
    throw new Error('User not registered');
  }
  const validPassword = await argon2.verify(userRecord.Item.password.S, password);

  if(validPassword) {
    const token = randomBytes(32).toString('hex');

    await RedisService.setElement({ key: token, value: userRecord?.Item?.email.S, ttl: 60 * 60 * 10 });
    
    return { email, token };
  } else {
    throw new Error('Invalid Password');
  }
};

export default blAuthorizeUser;