import RedisService from '../RedisService';
import { createUser } from 'repositories/users';

const blCreateUser = async ({ name, email, password }) => {
  
  const { name: nameToReturn, email: emailToReturn, token: accessToken } = await createUser({ name, email, password });
  
  await RedisService.setElement({ key: accessToken, value: email, ttl: 60 * 60 * 10 });

  return { nameToReturn, emailToReturn, accessToken };
};


export default blCreateUser;