import { createOrUpdateScreen } from 'repositories/screens';

const blCreateOrUpdateScreen = async ({ id, name, widgetsAvailable, currentUser }) => {

  return await createOrUpdateScreen({ id, name, widgetsAvailable, currentUser });
};


export default blCreateOrUpdateScreen;