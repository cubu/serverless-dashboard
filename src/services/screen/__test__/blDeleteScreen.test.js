import blDeleteScreen from '../blDeleteScreen';
import screenRepositoriesMock from '../../../repositories/screens';

const params = {
  id: 'uuid',
};

jest.mock('../../../repositories/screens', () => ({
  deleteScreen: jest.fn().mockResolvedValue({}),
}));


describe('blDeleteScreen', () => {
  it('Should Call Delete With correct params', async () => {
    await blDeleteScreen(params);

    expect(screenRepositoriesMock.deleteScreen).toBeCalledWith(expect.objectContaining({ id: params.id }));
  });

});
