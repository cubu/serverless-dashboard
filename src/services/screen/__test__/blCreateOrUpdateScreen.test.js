import blCreateOrUpdateScreen from '../blCreateOrUpdateScreen';
import screenRepositoriesMock from '../../../repositories/screens';

const params = {
  id: 'uuid',
  name: 'Pepe',
  widgetsAvailable: ['string'],
  currentUser: 'pepe@mail.com',
};

jest.mock('../../../repositories/screens', () => ({
  createOrUpdateScreen: jest.fn().mockResolvedValue({}),
}));


describe('blCreateOrUpdateScreen', () => {
  it('Should Call Create or Update With correct params', async () => {
    await blCreateOrUpdateScreen(params);

    expect(screenRepositoriesMock.createOrUpdateScreen).toBeCalledWith(expect.objectContaining({ id: params.id, name: params.name, widgetsAvailable: params.widgetsAvailable, currentUser: params.currentUser }));
  });

});
