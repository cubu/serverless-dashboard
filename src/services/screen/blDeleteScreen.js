import { deleteScreen } from 'repositories/screens';

const blDeleteScreen = async ({ id }) => {

  await deleteScreen({ id });
};

export default blDeleteScreen;