import { deleteSolution } from 'repositories/solutions';

const blDeleteSolution = async ({ id }) => {
  await deleteSolution({ id });
};


export default blDeleteSolution;