import blDeleteSolution from '../blDeleteSolution';
import solutionRepositoriesMock from '../../../repositories/solutions';

const params = {
  id: 'uuid',
};

jest.mock('../../../repositories/solutions', () => ({
  deleteSolution: jest.fn().mockResolvedValue({}),
}));


describe('blDeleteSolution', () => {
  it('Should Call Delete With correct params', async () => {
    await blDeleteSolution(params);

    expect(solutionRepositoriesMock.deleteSolution).toBeCalledWith(expect.objectContaining({ id: params.id }));
  });

});
