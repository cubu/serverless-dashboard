import blCreateOrUpdateSolution from '../blCreateOrUpdateSolution';
import solutionRepositoriesMock from '../../../repositories/solutions';

const params = {
  id: 'uuid',
  name: 'Pepe',
  screensAvailable: ['string'],
  currentUser: 'pepe@mail.com',
};

jest.mock('../../../repositories/solutions', () => ({
  createOrUpdateSolution: jest.fn().mockResolvedValue({}),
}));


describe('blCreateOrUpdateSolution', () => {
  it('Should Call Create or Update With correct params', async () => {
    await blCreateOrUpdateSolution(params);

    expect(solutionRepositoriesMock.createOrUpdateSolution).toBeCalledWith(expect.objectContaining({ id: params.id, name: params.name, screensAvailable: params.screensAvailable, currentUser: params.currentUser }));
  });

});
