import { createOrUpdateSolution } from 'repositories/solutions';

const blCreateOrUpdateSolution = async ({ id, name, screensAvailable, currentUser }) => {

  return await createOrUpdateSolution({ id, name, screensAvailable, currentUser });
  // Note: by abstracting the persist of data in another layer we are able to
  // change it more easily in the future(for instance to mongo) and now the business logic is more readable

  // Here for example we could issue an event towards SQS, Kinesis, Rabbit...
  // Announcing that a solution has been created
};


export default blCreateOrUpdateSolution;