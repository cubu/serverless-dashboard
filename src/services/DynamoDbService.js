import Dynamodb from 'aws-sdk/clients/dynamodb';
import { dynamodbConfig } from 'config';

class DynamoDbService {
  constructor() {
    this.config = {
      region: dynamodbConfig.region,
      endpoint: dynamodbConfig.endpoint,
    };

    this.client = null;
  }

  getClient() {
    if (!this.client) {
      this.client = new Dynamodb(this.config);
    }

    return this.client;
  }

  async putItem({ item, tableName }) {
    const params = {
      TableName: tableName,
      Item: item,
    };
    
    return await this.getClient().putItem(params).promise();
  }

  async getItem({ key, tableName }) {
    const params = {
      TableName: tableName,
      Key: key,
    };
    
    return this.getClient().getItem(params).promise();
  }

  async deleteItem({ key, tableName }) {
    const params = {
      TableName: tableName,
      Key: key,
    };
    
    return this.getClient().deleteItem(params).promise();
  }

  async updateItem({ key, updateExpression, expressionAttributeValues, tableName }) {
    const params = {
      TableName: tableName,
      Key: key,
      UpdateExpression: updateExpression,
      ExpressionAttributeValues: expressionAttributeValues,
      ReturnValues: 'ALL_NEW',
    };
    
    return await this.getClient().updateItem(params).promise();
  }
}

export default new DynamoDbService();
