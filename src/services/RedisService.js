import asyncRedis from 'async-redis';

import { redisConfig } from 'config';

class RedisService {
  constructor() {
    this.config = {
      host: redisConfig.host,
      port: redisConfig.port,
    };
    this.client = null;
  }

  getClient() {
    if (!this.client) {
      this.client = asyncRedis.createClient(this.config);
    }
    
    return this.client;
  }
  
  async setElement({ key, value, ttl = 60 * 60 * 10 }) {
    
    await this.getClient().set(key, JSON.stringify(value), 'EX', ttl);
  }

  async getElement({ key }) {
    return await this.getClient().get(key);
  }

}

export default new RedisService();
