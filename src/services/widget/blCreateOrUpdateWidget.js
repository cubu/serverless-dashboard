import { createOrUpdateWidget } from 'repositories/widgets';

const blCreateOrUpdateWidget = async ({ id, name, metric, kind, currentUser }) => {

  return await createOrUpdateWidget({ id, name, metric, kind, currentUser });
};


export default blCreateOrUpdateWidget;