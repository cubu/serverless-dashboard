import blDeleteWidget from '../blDeleteWidget';
import widgetRepositoriesMock from '../../../repositories/widgets';

const params = {
  id: 'uuid',
};

jest.mock('../../../repositories/widgets', () => ({
  deleteWidget: jest.fn().mockResolvedValue({}),
}));


describe('blDeleteWidget', () => {
  it('Should Call Delete With correct params', async () => {
    await blDeleteWidget(params);

    expect(widgetRepositoriesMock.deleteWidget).toBeCalledWith(expect.objectContaining({ id: params.id }));
  });

});
