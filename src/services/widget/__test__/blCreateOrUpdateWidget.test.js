import blCreateOrUpdateWidget from '../blCreateOrUpdateWidget';
import widgetRepositoriesMock from '../../../repositories/widgets';

const params = {
  id: 'uuid',
  name: 'Pepe',
  metric: 'prometheus_ds_gets',
  kind: 'Pie',
  currentUser: 'pepe@mail.com',
};

jest.mock('../../../repositories/widgets', () => ({
  createOrUpdateWidget: jest.fn().mockResolvedValue({}),
}));


describe('blCreateOrUpdateWidget', () => {
  it('Should Call Create or Update With correct params', async () => {
    await blCreateOrUpdateWidget(params);

    expect(widgetRepositoriesMock.createOrUpdateWidget).toBeCalledWith(expect.objectContaining({ id: params.id, name: params.name, metric: params.metric, kind: params.kind, currentUser: params.currentUser }));
  });
});
