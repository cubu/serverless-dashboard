import { deleteWidget } from 'repositories/widgets';

const blDeleteWidget = async ({ id }) => {

  await deleteWidget({ id });
};


export default blDeleteWidget;